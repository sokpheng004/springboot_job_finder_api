package online.hackpi.job_finder_spring_boot_api.base;

import lombok.Builder;
import org.springframework.http.HttpStatus;

import java.util.Date;


@Builder
public record MyBaseResponse<T>(
        Date date,
        Integer httpStatus,
        String message,
        T data
) {
}
