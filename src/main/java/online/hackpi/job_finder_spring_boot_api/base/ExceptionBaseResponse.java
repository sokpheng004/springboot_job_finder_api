package online.hackpi.job_finder_spring_boot_api.base;

import lombok.Builder;

@Builder
public record ExceptionBaseResponse<T>(
    Integer status,
    String date,
    String message
) {}
