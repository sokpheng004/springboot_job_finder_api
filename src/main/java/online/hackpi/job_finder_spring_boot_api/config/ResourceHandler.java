package online.hackpi.job_finder_spring_boot_api.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class ResourceHandler implements WebMvcConfigurer {
    @Value("${file.server.file-path}")
    String serverPath;
    @Value("${file.client.file-path}")
    String clientPathForAccess;
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler(clientPathForAccess)
                .addResourceLocations("file:"+serverPath);
    }
}
