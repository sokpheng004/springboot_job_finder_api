package online.hackpi.job_finder_spring_boot_api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication

public class JobFinderSpringBootApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(JobFinderSpringBootApiApplication.class, args);
    }

}
