package online.hackpi.job_finder_spring_boot_api.api.v1.job.model.dto;

import io.swagger.v3.oas.annotations.media.Schema;

import java.util.Set;

public record JobCreateDto(
        @Schema(defaultValue = "Senior web developer")
        String jobTitle,
        @Schema(defaultValue = """
                We're hiring for senior API developer.
                """)
        String jobDescription,
        @Schema(defaultValue = """
                /api/v1/files/aafsdghsf23432.png
                """)
        String jobInstruction,
        @Schema(defaultValue = """
                /api/v1/files/aafs2.png
                """)
        String jobThumbnail,
        Boolean isFullTime,
        Set<JobCategoryDto> jobCategorySet
) {
}
