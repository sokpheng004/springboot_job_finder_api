package online.hackpi.job_finder_spring_boot_api.api.v1.role.model;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class RoleService {
    private final RoleRepository repository;
    public List<Role> getAllRoles(){
        return repository.findAll();
    }
}
