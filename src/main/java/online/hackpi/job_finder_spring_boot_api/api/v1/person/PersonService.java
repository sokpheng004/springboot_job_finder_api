package online.hackpi.job_finder_spring_boot_api.api.v1.person;

import online.hackpi.job_finder_spring_boot_api.api.v1.person.model.dto.PersonCreateDto;
import online.hackpi.job_finder_spring_boot_api.api.v1.person.model.dto.PersonResponseDto;
import online.hackpi.job_finder_spring_boot_api.api.v1.exception.NotFoundExceptionConfig;

import java.util.List;


public interface PersonService {
    List<PersonResponseDto> getAllUser ();
    PersonResponseDto createNewUser(PersonCreateDto seeker);
    PersonResponseDto searchPersonByUuid(String uuid) throws NotFoundExceptionConfig;
}
