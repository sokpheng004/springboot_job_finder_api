package online.hackpi.job_finder_spring_boot_api.api.v1.auth.model.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

@Data
public class AuthCreateDto {
    @Schema(defaultValue = "kimchansokpheng123@gmail.com")
    private String email;
    @Schema(defaultValue = "123")
    private String password;
}
