package online.hackpi.job_finder_spring_boot_api.api.v1.job;

import lombok.RequiredArgsConstructor;
import online.hackpi.job_finder_spring_boot_api.api.v1.job.model.dto.JobCreateDto;
import online.hackpi.job_finder_spring_boot_api.api.v1.exception.NotFoundExceptionConfig;
import online.hackpi.job_finder_spring_boot_api.base.MyBaseResponse;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.time.Instant;
import java.util.Date;

@RestController
@RequestMapping("/api/v1/jobs")
@RequiredArgsConstructor
public class JobRestController {
    private final JobService jobService;
    @PostMapping
    public MyBaseResponse<Object> createNewJob(@RequestBody JobCreateDto job){
        return MyBaseResponse
                .builder()
                .httpStatus(HttpStatus.CREATED.value())
                .date(Date.from(Instant.now()))
                .data("New Job has been created.")
                .data(jobService.createNewJob(job))
                .build();
    }
    @GetMapping()
    public MyBaseResponse<Object> getAllJob(){
        return MyBaseResponse
                .builder()
                .httpStatus(HttpStatus.CREATED.value())
                .date(Date.from(Instant.now()))
                .message("Jobs are founded.")
                .data(jobService.getAllJob())
                .build();
    }
    @GetMapping(value = "/search")
    public MyBaseResponse<Object> searchPersonByIUuid(@RequestParam String id) throws NotFoundExceptionConfig {
        return MyBaseResponse
                .builder()
                .httpStatus(HttpStatus.FOUND.value())
                .message("Job is founded.")
                .date(Date.from(Instant.now()))
                .data(jobService.searchJobUuid(id))
                .build();
    }
}
