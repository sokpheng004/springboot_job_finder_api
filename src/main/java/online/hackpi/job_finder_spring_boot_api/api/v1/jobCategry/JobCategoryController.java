package online.hackpi.job_finder_spring_boot_api.api.v1.jobCategry;

import jakarta.websocket.server.PathParam;
import lombok.RequiredArgsConstructor;
import online.hackpi.job_finder_spring_boot_api.api.v1.exception.NotFoundExceptionConfig;
import online.hackpi.job_finder_spring_boot_api.base.MyBaseResponse;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.Instant;
import java.util.Date;

@RestController
@RequestMapping("/api/v1/categories")
@RequiredArgsConstructor
public class JobCategoryController {
    private final JobCategoryService jobCategoryService;
    @GetMapping
    public MyBaseResponse<Object> getAllJobCategories(){
        return MyBaseResponse
                .builder()
                .httpStatus(HttpStatus.FOUND.value())
                .date(Date.from(Instant.now()))
                .message("All job categories are found.")
                .data(jobCategoryService.getAllJobCategories())
                .build();
    }
    @GetMapping("name")
    public MyBaseResponse<Object> getCategoryByName(@PathParam("s") String name) throws NotFoundExceptionConfig {
        return MyBaseResponse
                .builder()
                .httpStatus(HttpStatus.FOUND.value())
                .date(Date.from(Instant.now()))
                .message("Category is found")
                .data(jobCategoryService.getCategoryByName(name))
                .build();
    }
}
