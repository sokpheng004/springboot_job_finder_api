package online.hackpi.job_finder_spring_boot_api.api.v1.apply.model.dto;

public record ApplyCreateDto(
        String personUuid,
        String jobUuid,
        String cvUrl
) {
}
