package online.hackpi.job_finder_spring_boot_api.api.v1.organization;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Organization {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String uuid;
    private String orgName;
    private String email;
    private String password;
    private String profile;
    private String referenceFile;
    private String address;
    private Date createdDate;
//
    private Boolean isDeleted;
    private Boolean isVerified;
}
