package online.hackpi.job_finder_spring_boot_api.api.v1.jobCategry;

import lombok.RequiredArgsConstructor;
import online.hackpi.job_finder_spring_boot_api.api.v1.AllMapStructs;
import online.hackpi.job_finder_spring_boot_api.api.v1.exception.NotFoundExceptionConfig;
import online.hackpi.job_finder_spring_boot_api.api.v1.jobCategry.model.dto.JobCategory;
import online.hackpi.job_finder_spring_boot_api.api.v1.jobCategry.model.dto.JobCategoryResponseDto;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class JobCategoryService {
    private final JobCategoryRepository categoryRepository;
    private final AllMapStructs mapStructs;
    public List<JobCategoryResponseDto> getAllJobCategories(){
        return mapStructs.mapFromListCategoryToListOfJobCategoryResponseDto(categoryRepository.findAll());
    }
    public JobCategory getCategoryByName(String name) throws NotFoundExceptionConfig{
        var data = categoryRepository.findJobCategoriesByCategoryName(name);
        if(data==null){
            throw new NotFoundExceptionConfig("Category not found with type: " + name);
        }
        return data ;
    }
}
