package online.hackpi.job_finder_spring_boot_api.api.v1.apply.model;

import lombok.RequiredArgsConstructor;
import online.hackpi.job_finder_spring_boot_api.api.v1.apply.model.dto.ApplyCreateDto;
import online.hackpi.job_finder_spring_boot_api.api.v1.exception.NotFoundExceptionConfig;
import online.hackpi.job_finder_spring_boot_api.base.MyBaseResponse;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.time.Instant;
import java.util.Date;

@RestController
@RequestMapping("/api/v1/applies")
@RequiredArgsConstructor
public class ApplyRestController {
    private final ApplyService applyService;
    @GetMapping()
    public MyBaseResponse<?> getAllApply(){
        return MyBaseResponse
                .builder()
                .httpStatus(HttpStatus.CREATED.value())
                .date(Date.from(Instant.now()))
                .message("All applies have been founded.")
                .data(applyService.getAllApply())
                .build();
    }
    @PostMapping()
    public MyBaseResponse<?> applyForAJob(@RequestBody ApplyCreateDto applyCreateDto){
        return MyBaseResponse
                .builder()
                .httpStatus(HttpStatus.CREATED.value())
                .date(Date.from(Instant.now()))
                .message("User has been applied for a job.")
                .data(applyService.apply(applyCreateDto))
                .build();
    }
    @GetMapping("/search")
    public MyBaseResponse<?> searchApplyByUuid(@RequestParam String id) throws NotFoundExceptionConfig {
        return MyBaseResponse
                .builder()
                .httpStatus(HttpStatus.FOUND.value())
                .date(Date.from(Instant.now()))
                .message("Apply has been founded.")
                .data(applyService.searchApplyBYUuid(id))
                .build();
    }
}
