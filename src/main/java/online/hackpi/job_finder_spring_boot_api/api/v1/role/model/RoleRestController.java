package online.hackpi.job_finder_spring_boot_api.api.v1.role.model;

import lombok.RequiredArgsConstructor;
import online.hackpi.job_finder_spring_boot_api.base.MyBaseResponse;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.Instant;
import java.util.Date;


@RestController
@RequestMapping("/api/v1/roles")
@RequiredArgsConstructor
public class RoleRestController {
    private final RoleService roleService;
    @GetMapping()
    public MyBaseResponse<Object> getAllRoles(){
        return MyBaseResponse
                .builder()
                .httpStatus(HttpStatus.FOUND.value())
                .date(Date.from(Instant.now()))
                .message("Roles are founded")
                .data(roleService.getAllRoles())
                .build();
    }
}
