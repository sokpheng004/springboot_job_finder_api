package online.hackpi.job_finder_spring_boot_api.api.v1.job.model.dto;

import java.util.Date;
import java.util.Set;

public record JobResponseDto(
        String uuid,
        String jobTitle,
        String jobDescription,
        String jobInstruction,
        String jobThumbnail,
        Date createdDate,
        Boolean isFullTime,
        Boolean isDeleted,
        Boolean isVerified,
        Set<JobCategoryDto> jobCategorySet
) {
}
