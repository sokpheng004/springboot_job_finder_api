package online.hackpi.job_finder_spring_boot_api.api.v1.job.model;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import online.hackpi.job_finder_spring_boot_api.api.v1.jobCategry.model.dto.JobCategory;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Job {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String uuid;

    private String jobTitle;
    private String jobDescription;
    private String jobInstruction;
    private String jobThumbnail;
    private Date createdDate;
    private Boolean isFullTime;
    private Boolean isDeleted;
    private Boolean isVerified;
//
    @ManyToMany
    @JoinTable(name = "job_categories",
    joinColumns = @JoinColumn(name = "job_id"),
    inverseJoinColumns = @JoinColumn(name = "category _id"))
    private Set<JobCategory> jobCategorySet = new HashSet<>();
}
