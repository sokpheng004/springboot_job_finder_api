package online.hackpi.job_finder_spring_boot_api.api.v1.person.model.dto;

import online.hackpi.job_finder_spring_boot_api.api.v1.role.model.Role;
import online.hackpi.job_finder_spring_boot_api.api.v1.role.model.dto.RoleDto;

import java.util.HashSet;
import java.util.Set;

public record PersonCreateDto(
        String personName,
        String email,
        String password,
        Set<RoleDto> roles
) { }
