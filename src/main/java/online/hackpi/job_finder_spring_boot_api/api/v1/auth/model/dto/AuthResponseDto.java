package online.hackpi.job_finder_spring_boot_api.api.v1.auth.model.dto;
import lombok.Builder;

@Builder
public record AuthResponseDto(
        String accessToken,
        String refreshToken
) {
}
