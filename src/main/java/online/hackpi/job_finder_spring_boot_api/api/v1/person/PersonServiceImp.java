package online.hackpi.job_finder_spring_boot_api.api.v1.person;


import lombok.RequiredArgsConstructor;
import online.hackpi.job_finder_spring_boot_api.api.v1.AllMapStructs;
import online.hackpi.job_finder_spring_boot_api.api.v1.person.model.Person;
import online.hackpi.job_finder_spring_boot_api.api.v1.person.model.dto.PersonCreateDto;
import online.hackpi.job_finder_spring_boot_api.api.v1.person.model.dto.PersonResponseDto;
import online.hackpi.job_finder_spring_boot_api.api.v1.role.model.RoleRepository;
import online.hackpi.job_finder_spring_boot_api.api.v1.exception.NotFoundExceptionConfig;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class PersonServiceImp implements PersonService {

    private final PersonRepository personRepository;

    private final AllMapStructs allMapStructs;
    private final RoleRepository repository;
    @Override
    public List<PersonResponseDto> getAllUser() {
        return allMapStructs.mapFromPersonListToPersonListDto(personRepository.findAll());
    }
    @Override
    public PersonResponseDto createNewUser(PersonCreateDto seeker) {
        Person person1 = new Person();
        person1.setUuid(UUID.randomUUID().toString());
        person1.setIsDeleted(false);
        person1.setIsVerified(true);
        person1.setPassword(seeker.password());
        person1.setEmail(seeker.email());
        person1.setPersonName(seeker.personName());
        person1.setCreatedDate(Date.from(Instant.now()));
        seeker.roles().forEach(e->{
            person1.getRoles().add(repository.findRoleByRoleName(e.roleName().toUpperCase()));
        });
//        return allMapStructs.mapFromSeekerToSeekerDto(seeker1);
        return allMapStructs.mapFromPersonToPersonDto(personRepository.save(person1));
    }

    @Override
    public PersonResponseDto searchPersonByUuid(String uuid) throws NotFoundExceptionConfig {
        var data = allMapStructs.mapFromPersonToPersonDto(personRepository.findByUuid(uuid));
        if (data==null){
            throw new NotFoundExceptionConfig("User is not found with id: " + uuid);
        }
        return data;
    }
}
