package online.hackpi.job_finder_spring_boot_api.api.v1.person.model.dto;


import online.hackpi.job_finder_spring_boot_api.api.v1.role.model.dto.RoleDto;

import java.util.Date;
import java.util.Set;

public record PersonResponseDto(
        String uuid,
        String personName,
        String email,
        String profile,
        String bio,
        String address,
        Date createdDate,
        Boolean isDeleted,
        Boolean isVerified,
        Set<RoleDto> roles
) { }
