package online.hackpi.job_finder_spring_boot_api.api.v1.file;

import lombok.RequiredArgsConstructor;
import online.hackpi.job_finder_spring_boot_api.api.v1.file.dto.FileResponseDto;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class FileService {
    @Value("${file.server.file-path}")
    String serverPath;
    @Value("${file.client.file-path}")
    String clientPathForAccess;
    @Value("${file.client.preview}")
    String previewUrl;
    public FileResponseDto uploadSingle(MultipartFile multipartFile){
        return uploadFile(multipartFile);
    }
    public List<FileResponseDto> uploadMultiFiles(MultipartFile [] files){
        List<FileResponseDto> fileResponseDtoList = new ArrayList<>();
        for(MultipartFile file: files){
            fileResponseDtoList.add(uploadFile(file));
        }
        return fileResponseDtoList;
    }
    private FileResponseDto uploadFile(MultipartFile multipartFile){
        int lastDotIndex = Objects.requireNonNull(multipartFile.getOriginalFilename()).lastIndexOf(".");
        String extension = multipartFile.getOriginalFilename().substring(lastDotIndex+1);
        long size = multipartFile.getSize();
        String name = String.format("%s.%s", UUID.randomUUID(), extension);
        String url = String.format("%s%s", previewUrl, name);
        Path path = Paths.get(serverPath + name);
        try{
            Files.copy(multipartFile.getInputStream(), path);
        }catch (IOException exception){
            System.out.println("Problem during upload file: " + exception.getMessage());
        }
        return FileResponseDto.builder()
                .originalFileName(multipartFile.getOriginalFilename())
                .fileName(name)
                .fileUrl(url)
                .size(size)
                .build();
    }
}
