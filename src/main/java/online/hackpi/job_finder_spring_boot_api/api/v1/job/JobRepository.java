package online.hackpi.job_finder_spring_boot_api.api.v1.job;

import online.hackpi.job_finder_spring_boot_api.api.v1.job.model.Job;
import org.springframework.data.jpa.repository.JpaRepository;

public interface JobRepository extends JpaRepository<Job, Integer> {
    Job findByUuid(String uuid);
}
