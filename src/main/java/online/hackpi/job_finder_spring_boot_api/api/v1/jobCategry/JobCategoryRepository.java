package online.hackpi.job_finder_spring_boot_api.api.v1.jobCategry;


import online.hackpi.job_finder_spring_boot_api.api.v1.jobCategry.model.dto.JobCategory;
import org.springframework.data.jpa.repository.JpaRepository;

public interface JobCategoryRepository extends JpaRepository<JobCategory, Integer> {
    JobCategory findJobCategoriesByCategoryName(String name);
}
