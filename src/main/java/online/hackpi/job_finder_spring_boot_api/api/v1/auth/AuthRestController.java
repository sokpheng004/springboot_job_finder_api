package online.hackpi.job_finder_spring_boot_api.api.v1.auth;

import lombok.RequiredArgsConstructor;

import online.hackpi.job_finder_spring_boot_api.api.v1.auth.model.dto.AuthCreateDto;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/auth")
@RequiredArgsConstructor
public class AuthRestController {
    @PostMapping("/login")
    public ResponseEntity<?> login(@RequestBody AuthCreateDto authCreateDto) throws Exception {
        return null;
    }

}
