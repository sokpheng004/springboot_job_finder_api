package online.hackpi.job_finder_spring_boot_api.api.v1.utils;

import online.hackpi.job_finder_spring_boot_api.api.v1.exception.NotFoundExceptionConfig;
import online.hackpi.job_finder_spring_boot_api.base.ExceptionBaseResponse;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.time.Instant;
import java.util.Date;


@RestControllerAdvice
@Configuration
public class ExceptionHandlingConfig {
    @ExceptionHandler(NotFoundExceptionConfig.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ExceptionBaseResponse<?> entityNotFound(NotFoundExceptionConfig exceptionConfig){
        return ExceptionBaseResponse
                .builder()
                .status(HttpStatus.NOT_FOUND.value())
                .date(Date.from(Instant.now()).toString())
                .message(exceptionConfig.getMessage())
                .build();
    }
}
