package online.hackpi.job_finder_spring_boot_api.api.v1.person;

import online.hackpi.job_finder_spring_boot_api.api.v1.person.model.Person;
import org.springframework.data.jpa.repository.JpaRepository;


public interface PersonRepository extends JpaRepository<Person, Integer> {
    Person findByUuid(String uuid);
}
