package online.hackpi.job_finder_spring_boot_api.api.v1.apply.model;


import jakarta.persistence.*;
import lombok.Data;
import online.hackpi.job_finder_spring_boot_api.api.v1.job.model.Job;
import online.hackpi.job_finder_spring_boot_api.api.v1.person.model.Person;

import java.util.Date;

@Entity
@Data
public class Apply {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String uuid;
    private Date appliedDate;
    @ManyToOne
    private Job job;
    @ManyToOne
    private Person person;
    String cvUrl;
}
