package online.hackpi.job_finder_spring_boot_api.api.v1.apply.model.dto;

import online.hackpi.job_finder_spring_boot_api.api.v1.job.model.dto.JobResponseDto;
import online.hackpi.job_finder_spring_boot_api.api.v1.person.model.dto.PersonResponseDto;
import java.util.Date;

public record ApplyResponseDto(
        String uuid,
        Date appliedDate,
        PersonResponseDto person,
        JobResponseDto job,
        String cvUrl
) {
}
