package online.hackpi.job_finder_spring_boot_api.api.v1.jobCategry.model.dto;

import lombok.Builder;

@Builder
public record JobCategoryResponseDto(
        String categoryName
) {
}
