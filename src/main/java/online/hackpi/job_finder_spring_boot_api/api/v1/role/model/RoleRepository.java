package online.hackpi.job_finder_spring_boot_api.api.v1.role.model;

import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role, Integer> {
    Role findRoleByRoleName(String name);
}
