package online.hackpi.job_finder_spring_boot_api.api.v1.job;

import lombok.RequiredArgsConstructor;
import online.hackpi.job_finder_spring_boot_api.api.v1.AllMapStructs;
import online.hackpi.job_finder_spring_boot_api.api.v1.job.model.Job;
import online.hackpi.job_finder_spring_boot_api.api.v1.job.model.dto.JobCreateDto;
import online.hackpi.job_finder_spring_boot_api.api.v1.job.model.dto.JobResponseDto;
import online.hackpi.job_finder_spring_boot_api.api.v1.jobCategry.model.dto.JobCategory;
import online.hackpi.job_finder_spring_boot_api.api.v1.jobCategry.JobCategoryRepository;
import online.hackpi.job_finder_spring_boot_api.api.v1.exception.NotFoundExceptionConfig;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class JobServiceImp implements JobService{
    private final JobRepository repository;
    private final AllMapStructs allMapStructs;
    private final JobCategoryRepository jobCategoryRepository;
    @Override
    public JobResponseDto createNewJob(JobCreateDto jobCreateDto) {
        Job job1 = allMapStructs.mapFromJobDtoToJob(allMapStructs.mapFromJobCreateDtoToJobDto(jobCreateDto));
        job1.setJobCategorySet(new HashSet<>());
        job1.setUuid(UUID.randomUUID().toString());
        job1.setCreatedDate(Date.from(Instant.now()));
        job1.setIsDeleted(false);
        job1.setIsVerified(true);
        jobCreateDto.jobCategorySet().forEach(e->{
            JobCategory jobCategory = jobCategoryRepository.findJobCategoriesByCategoryName(e.categoryName().toLowerCase());
            if(jobCategory!=null){
                job1.getJobCategorySet().add(jobCategory);
            }
        });
        return allMapStructs.mapFromJobToJobDto(repository.save(job1));
    }

    @Override
    public List<JobResponseDto>getAllJob() {
        return allMapStructs.mapFromListOfJobToListOfJobResponseDto(repository.findAll());
    }

    @Override
    public JobResponseDto searchJobUuid(String uuid) throws NotFoundExceptionConfig {
        var data = allMapStructs.mapFromJobToJobDto(repository.findByUuid(uuid));
        if(data==null){
            throw  new NotFoundExceptionConfig("Job not founded with id: " + uuid);
        }
        return data;
    }
}
