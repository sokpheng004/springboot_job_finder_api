package online.hackpi.job_finder_spring_boot_api.api.v1.job.model.dto;

public record JobCategoryDto(
        String categoryName
) {
}
