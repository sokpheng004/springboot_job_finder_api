package online.hackpi.job_finder_spring_boot_api.api.v1.exception;

public class NotFoundExceptionConfig extends Exception{
    public NotFoundExceptionConfig(){}
    public NotFoundExceptionConfig(String message){
        super(message);
    }
}
