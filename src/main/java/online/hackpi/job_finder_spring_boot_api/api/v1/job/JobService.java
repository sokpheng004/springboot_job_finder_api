package online.hackpi.job_finder_spring_boot_api.api.v1.job;


import online.hackpi.job_finder_spring_boot_api.api.v1.job.model.dto.JobCreateDto;
import online.hackpi.job_finder_spring_boot_api.api.v1.job.model.dto.JobResponseDto;
import online.hackpi.job_finder_spring_boot_api.api.v1.exception.NotFoundExceptionConfig;

import java.util.List;

public interface JobService {
    JobResponseDto createNewJob(JobCreateDto job);
    List<JobResponseDto> getAllJob();
    JobResponseDto searchJobUuid(String uuid) throws NotFoundExceptionConfig;
}
