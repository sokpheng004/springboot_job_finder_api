package online.hackpi.job_finder_spring_boot_api.api.v1.role.model.dto;

public record RoleDto (String roleName){
}
