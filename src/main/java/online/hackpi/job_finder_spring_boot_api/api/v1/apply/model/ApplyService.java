package online.hackpi.job_finder_spring_boot_api.api.v1.apply.model;

import lombok.RequiredArgsConstructor;
import online.hackpi.job_finder_spring_boot_api.api.v1.AllMapStructs;
import online.hackpi.job_finder_spring_boot_api.api.v1.apply.model.dto.ApplyCreateDto;
import online.hackpi.job_finder_spring_boot_api.api.v1.apply.model.dto.ApplyResponseDto;
import online.hackpi.job_finder_spring_boot_api.api.v1.job.JobRepository;
import online.hackpi.job_finder_spring_boot_api.api.v1.person.PersonRepository;
import online.hackpi.job_finder_spring_boot_api.api.v1.exception.NotFoundExceptionConfig;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class ApplyService {
    private final ApplyRepository applyRepository;
    private final PersonRepository personRepository;
    private final JobRepository jobRepository;
    private final AllMapStructs allMapStructs;
    public ApplyResponseDto apply(ApplyCreateDto apply){
        Apply apply1 = new Apply();
        apply1.setUuid(UUID.randomUUID().toString());
        apply1.setAppliedDate(Date.from(Instant.now()));
        apply1.setJob(jobRepository.findByUuid(apply.jobUuid()));
        apply1.setPerson(personRepository.findByUuid(apply.personUuid()));
        apply1.setCvUrl(apply.cvUrl());
        return allMapStructs.mapFromApplyToApplyDto(applyRepository.save(apply1));
    }
    public ApplyResponseDto searchApplyBYUuid(String uuid) throws NotFoundExceptionConfig {
        var data = allMapStructs.mapFromApplyToApplyDto(applyRepository.searchAppliesByUuid(uuid));
        if(data==null){
            throw new NotFoundExceptionConfig("Apply not founded with id: " + uuid);
        }
        return data;
    }
    public List<ApplyResponseDto> getAllApply(){
        return allMapStructs.mapFromApplyListToApplyResponseDtoList(applyRepository.findAll());
    }
}
