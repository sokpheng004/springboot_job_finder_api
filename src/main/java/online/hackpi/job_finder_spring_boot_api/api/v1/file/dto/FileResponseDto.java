package online.hackpi.job_finder_spring_boot_api.api.v1.file.dto;

import lombok.Builder;

@Builder
public record FileResponseDto(
        String originalFileName,
        String fileName,
        String fileUrl,
        String fileDownloadUrl,
        Long size
) {
}
