package online.hackpi.job_finder_spring_boot_api.api.v1.apply.model;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ApplyRepository extends JpaRepository<Apply, Integer> {
    Apply searchAppliesByUuid(String uuid);
}
