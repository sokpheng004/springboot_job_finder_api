package online.hackpi.job_finder_spring_boot_api.api.v1.person;


import lombok.RequiredArgsConstructor;
import online.hackpi.job_finder_spring_boot_api.api.v1.person.model.dto.PersonCreateDto;
import online.hackpi.job_finder_spring_boot_api.api.v1.exception.NotFoundExceptionConfig;
import online.hackpi.job_finder_spring_boot_api.base.MyBaseResponse;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.time.Instant;
import java.util.Date;

@RestController
@RequestMapping("/api/v1/person")
@RequiredArgsConstructor
public class PersonController {

    private final PersonService personService;

    @GetMapping
    public MyBaseResponse<Object> getAllPerson() {
        return MyBaseResponse
                .builder()
                .httpStatus(HttpStatus.FOUND.value())
                .date(Date.from(Instant.now()))
                .message("User are founded")
                .data(personService.getAllUser())
                .build();
    }

    @PostMapping
    public MyBaseResponse<Object> createNewPerson(@RequestBody PersonCreateDto seeker) {
        return MyBaseResponse
                .builder()
                .httpStatus(HttpStatus.CREATED.value())
                .date(Date.from(Instant.now()))
                .message("User is founded")
                .data(personService.createNewUser(seeker))
                .build();
    }
    @GetMapping("/search")
    public MyBaseResponse<Object> searchPersonByIUuid(@RequestParam String id) throws NotFoundExceptionConfig {
        return MyBaseResponse.builder()
                .httpStatus(HttpStatus.FOUND.value())
                .date(Date.from(Instant.now()))
                .message("User is founded")
                .data(personService.searchPersonByUuid(id))
                .build();
    }
}