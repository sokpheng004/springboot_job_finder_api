package online.hackpi.job_finder_spring_boot_api.api.v1;


import online.hackpi.job_finder_spring_boot_api.api.v1.apply.model.Apply;
import online.hackpi.job_finder_spring_boot_api.api.v1.apply.model.dto.ApplyResponseDto;
import online.hackpi.job_finder_spring_boot_api.api.v1.job.model.Job;
import online.hackpi.job_finder_spring_boot_api.api.v1.job.model.dto.JobCreateDto;
import online.hackpi.job_finder_spring_boot_api.api.v1.job.model.dto.JobResponseDto;
import online.hackpi.job_finder_spring_boot_api.api.v1.jobCategry.model.dto.JobCategory;
import online.hackpi.job_finder_spring_boot_api.api.v1.jobCategry.model.dto.JobCategoryResponseDto;
import online.hackpi.job_finder_spring_boot_api.api.v1.person.model.Person;
import online.hackpi.job_finder_spring_boot_api.api.v1.person.model.dto.PersonResponseDto;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface AllMapStructs {
    PersonResponseDto mapFromPersonToPersonDto(Person person);
    List<PersonResponseDto> mapFromPersonListToPersonListDto(List<Person> personList);
    JobResponseDto mapFromJobToJobDto(Job job);
    Job mapFromJobDtoToJob(JobResponseDto dto);
    JobResponseDto mapFromJobCreateDtoToJobDto(JobCreateDto jobCreateDto);
    ApplyResponseDto mapFromApplyToApplyDto(Apply apply);
    List<JobResponseDto> mapFromListOfJobToListOfJobResponseDto(List<Job> jobList);
    List<ApplyResponseDto> mapFromApplyListToApplyResponseDtoList(List<Apply> applyList);
    List<JobCategoryResponseDto> mapFromListCategoryToListOfJobCategoryResponseDto(List<JobCategory> jobCategoryList);
}
