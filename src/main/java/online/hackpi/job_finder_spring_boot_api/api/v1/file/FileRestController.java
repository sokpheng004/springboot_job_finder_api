package online.hackpi.job_finder_spring_boot_api.api.v1.file;

import lombok.RequiredArgsConstructor;
import online.hackpi.job_finder_spring_boot_api.api.v1.file.dto.FileResponseDto;
import online.hackpi.job_finder_spring_boot_api.base.MyBaseResponse;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.time.Instant;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/api/v1/files")
@RequiredArgsConstructor
public class FileRestController {
    private final FileService fileService;
    @PostMapping()
    public MyBaseResponse<FileResponseDto> uploadSingleFile(@RequestPart("file") MultipartFile file){
        return new MyBaseResponse<FileResponseDto>(
                Date.from(Instant.now()),
                HttpStatus.OK.value(),
                "File has been uploaded successfully",
                fileService.uploadSingle(file)
        );
    }
    @PostMapping("/multi-upload")
    public MyBaseResponse<List<FileResponseDto>> uploadMultiFiles(@RequestPart("files") MultipartFile [] files){
        return new MyBaseResponse<>(
                Date.from(Instant.now()),
                HttpStatus.OK.value(),
                "Files have been uploaded successfully",
                fileService.uploadMultiFiles(files)
        );
    }
}
